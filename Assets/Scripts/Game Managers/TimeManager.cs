using UnityEngine;
using System.Collections;

public class TimeManager {

    private float currentTimeScale = 0;
    private bool isPaused = false;

    public void PauseGame() {
        Time.timeScale = 0f;
        isPaused = true;
    }

    public void UnpauseGame() {
        Time.timeScale = 1f;
        isPaused = false;
    }

    public void UpdateTimeScale(float newTimeScale) {
        currentTimeScale = newTimeScale;
    }

    public float GetScaledDeltaTime(float deltaTime) {
        return (deltaTime * currentTimeScale);
    }

    public bool IsPaused() {
        return isPaused;
    }
}
