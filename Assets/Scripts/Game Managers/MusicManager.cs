using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

[RequireComponent(typeof(AudioSource))]
public class MusicManager : MonoBehaviour {


    public AudioClip[] musicByLevel;
    public static MusicManager instance;

    private AudioSource audioSource;
    
    // Use this for initialization
    void Awake() {
        if (instance != null && instance != this) {
            Destroy(gameObject);
        }
        else {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }

    void OnEnable() {
        SceneManager.sceneLoaded += OnLevelLoad;
    }

    void OnDisable() {
        SceneManager.sceneLoaded -= OnLevelLoad;
    }

    void Start() {
        audioSource = GetComponent<AudioSource>();
        audioSource.volume = PlayerPrefsManager.GetMasterVolume();
        PlayLevelMusic(SceneManager.GetActiveScene().buildIndex);
    }
	// Update is called once per frame
	void Update () {
	
	}

    void OnLevelLoad(Scene scene, LoadSceneMode mode) {
        PlayLevelMusic(scene.buildIndex);
    }

    void SetVolume(float newVolume) {
        if(audioSource) {
            audioSource.volume = newVolume;
        }
    }

    void PlayLevelMusic(int level) {
        AudioSource currentMusic = GetComponent<AudioSource>();
        AudioClip thisLevelMusic = musicByLevel[level];

        if (currentMusic.clip != thisLevelMusic && thisLevelMusic) {
            audioSource.clip = thisLevelMusic;
            audioSource.loop = true;
            audioSource.Play();
        }
    }
}
