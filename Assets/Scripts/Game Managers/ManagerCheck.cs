using UnityEngine;
using System.Collections;

public class ManagerCheck : MonoBehaviour {

    public GameObject musicPrefab;

    private MusicManager musicManager = null;

	// Use this for initialization
	void Start () {
        musicManager = FindObjectOfType<MusicManager>();
        if(!musicManager) {
            Instantiate(musicPrefab);
        }
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
