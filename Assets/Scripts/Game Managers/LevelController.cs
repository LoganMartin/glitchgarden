using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;


/// <summary>
/// Oh god this class needs refactoring
/// </summary>
public class LevelController : MonoBehaviour {

    public static LevelController instance = null;
    public GameObject guiCanvas;
    public GameObject endLevelDisplay;
    public LevelParameters[] levelParams;

    private TimeManager timeManager;
    private LevelParameters currentLevel;
    private LevelStats currentLevelStats;
    private Slider progressSlider;
    private Button pauseButton;
    private StarDisplay starDisplay;
    private EndLevelDisplay endDisplay;
    private GameObject levelOverDisplay;
    private GameObject levelGUI;
    private int homeBaseHealthRemaining;
    private float timeElapsed;
    private bool levelIsOver = false;

    //Ensure LevelController is a Singleton.
    void Awake() {
        if(instance != null && instance != this) {
            Destroy(gameObject);
        }
        else {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }

    void OnEnable() {
        SceneManager.sceneLoaded += OnLevelLoad;
    }

    void OnDisable() {
        SceneManager.sceneLoaded -= OnLevelLoad;
    }

    // Use this for initialization
    void Start () {
        currentLevel = levelParams[SceneManager.GetActiveScene().buildIndex - 3];
        InitializeLevel();
        //levelOverDisplay.SetActive(false);
    }
	
	// Update is called once per frame
	void Update () {
        timeElapsed += Time.deltaTime;
        UpdateLevelProgress();
    }

    void OnLevelLoad(Scene scene, LoadSceneMode mode) {
        //Ensure we're only calling this once, from our singleton.
        //Unity will by default call OnLevelWasLoaded on each instance of an object before we have a chance
        //To remove duplicates, ensuring singleton status.
        int level = scene.buildIndex;
        if (instance == this) {
            if (levelParams.Length > level - 3) {
                currentLevel = levelParams[level - 3];
                InitializeLevel();
            }
        }
    }

    void InitializeLevel() {

        if(timeManager == null) {
            timeManager = new TimeManager();
        }

        timeManager.UnpauseGame();
        levelIsOver = false;
        FindGUICanvas();
        FindLevelEndPanel();
        endDisplay.HideDisplay();
        ResetLevelStats();
        InitializeStarDisplay();
        homeBaseHealthRemaining = currentLevel.attackersBeforeLose;
        timeElapsed = 0;
        progressSlider = GameObject.Find("ProgressSlider").GetComponent<Slider>();
        pauseButton = GameObject.Find("PauseButton").GetComponent<Button>();
        progressSlider.value = 0f;
    }

    void GameOver() {
        levelIsOver = true;
        pauseButton.interactable = false;
        endDisplay.SetToLoseDisplay();
        endDisplay.UpdateStats(currentLevelStats);
        endDisplay.ShowDisplay();
        timeManager.PauseGame();
    }

    void WinLevel() {
        levelIsOver = true;
        pauseButton.interactable = false;
        endDisplay.SetToContinueDisplay();
        endDisplay.UpdateStats(currentLevelStats);
        endDisplay.ShowDisplay();
        timeManager.PauseGame();
    }

    public void HomeBaseReached() {
        homeBaseHealthRemaining--;
        UpdateStat(StatType.AttackersMissed, 1);
        if(homeBaseHealthRemaining <= 0 && !levelIsOver) {
            GameOver();
        }
    }

    void UpdateLevelProgress() {
        float currentProgress = Mathf.Clamp(timeElapsed / currentLevel.timeToWin, 0f, 1f);
        if(currentProgress >= 1 && !levelIsOver) {
            WinLevel();
            return;
        }

        if(progressSlider) {
            progressSlider.value = currentProgress;
        }
    }

    void FindGUICanvas() {
        levelGUI = GameObject.Find("ScreenSpace GUI");
        if(!levelGUI) {
            levelGUI = Instantiate(guiCanvas) as GameObject;
        }
    }

    void FindLevelEndPanel() {
        levelOverDisplay = GameObject.Find("LevelOver Panel");
        if (!levelOverDisplay) {
            levelOverDisplay = Instantiate(endLevelDisplay) as GameObject;
            levelOverDisplay.transform.SetParent(levelGUI.transform, false);
        }
        endDisplay = levelOverDisplay.GetComponent<EndLevelDisplay>();
    }

    void ResetLevelStats() {
        //Is there a way to delete old LevelStats now, or does garbage collection do well enough?
        currentLevelStats = new LevelStats();
    }

    public void UpdateStat(StatType statType, int change) {
        switch(statType) {
            case StatType.AttackersDefeated:
                currentLevelStats.attackersDefeated += change;
                break;
            case StatType.AttackersMissed:
                currentLevelStats.attackersMissed += change;
                break;
            case StatType.DefendersSpawned:
                currentLevelStats.defendersSpawned += change;
                break;
            case StatType.StarsCollected:
                currentLevelStats.starsCollected += change;
                break;
            case StatType.StarsSpent:
                currentLevelStats.starsSpent += change;
                break;
            default:
                break;
        }
    }

    void InitializeStarDisplay() {
        starDisplay = GameObject.Find("StarsText").GetComponent<StarDisplay>();
        starDisplay.InitializeDisplay(currentLevel.startingStars);
    }

    public void TogglePause() {
        if(timeManager.IsPaused()) {
            timeManager.UnpauseGame();
        }
        else {
            timeManager.PauseGame();
        }
    }
}
