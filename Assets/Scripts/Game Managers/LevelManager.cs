using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class LevelManager : MonoBehaviour {

    public bool autoLoadNext = false;
    public float autoLoadDelay;

    void Start() {
        if(autoLoadNext) {
            if(autoLoadDelay >= 0) {
                Invoke("LoadNextLevel", autoLoadDelay);
            }
            else {
                Debug.LogError("Error - Level Manager Script: Auto Load Delay must be 0 or greater");
            }
        }
    }

    public static void LoadLevel(string name) {
        //Debug.Log("Level Load Requested: " + name);
        //Application.LoadLevel(name);
        SceneManager.LoadScene(name);
    }

    public static void QuitRequest(string name) {
        Debug.Log("Quit Game Requested: " + name);

        // Bad Practice, only use for PC/Mac/Linux or Console builds
        Application.Quit();
    }

    public void LoadNextLevel() {
        //Application.LoadLevel(Application.loadedLevel + 1);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void uiNavigateToLevel(string levelName) {
        if(levelName != "Quit") {
            //Application.LoadLevel(levelName);
            SceneManager.LoadScene(levelName);
        }
        else {
            Application.Quit();
        }
    }

}
