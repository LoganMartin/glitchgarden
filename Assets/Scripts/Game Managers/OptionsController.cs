using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class OptionsController : MonoBehaviour {

    public Slider volumeSlider;
    public Slider difficultySlider;

    private MusicManager musicManager;
    private AudioSource currentMusic;
    private float currentVolume = 1f;
    private int currentDifficulty = 1;

	// Use this for initialization
	void Start () {
        musicManager = GameObject.FindObjectOfType<MusicManager>();
        if(musicManager) {
            currentMusic = musicManager.GetComponent<AudioSource>();
        }
        LoadSettings();
	}
	
    public void ChangeVolume(float volume) {
        currentVolume = volume;
        if (currentMusic) {
            currentMusic.volume = volume;
            currentVolume = volume;
        }
    }

    public void ChangeDifficulty(float difficulty) {
        currentDifficulty = (int)difficulty;
    }

    public void SaveSettings() {
        //Debug.Log("Saving Volume: " + currentVolume);
        //Debug.Log("Saving Difficulty: " + currentDifficulty);
        PlayerPrefsManager.SetMasterVolume(currentVolume);
        PlayerPrefsManager.SetDifficulty(currentDifficulty);
        //print(PlayerPrefsManager.GetMasterVolume());
    }

    public void LoadSettings() {
        //Debug.Log("Loaded Volume: " + PlayerPrefsManager.GetMasterVolume());
        //Debug.Log("Loaded Difficulty: " + PlayerPrefsManager.GetDifficulty());
        volumeSlider.value = PlayerPrefsManager.GetMasterVolume();
        difficultySlider.value = (float)PlayerPrefsManager.GetDifficulty();
        currentVolume = volumeSlider.value;
        currentDifficulty = (int)difficultySlider.value;
    }

    public void ResetToDefaultSettings() {
        ChangeVolume(1f);
        ChangeDifficulty(1f);
        SaveSettings();
        LoadSettings();
    }
}
