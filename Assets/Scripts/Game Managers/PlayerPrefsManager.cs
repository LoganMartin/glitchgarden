using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class PlayerPrefsManager : MonoBehaviour {

    const string MASTER_VOLUME_KEY = "master_volume";
    const string DIFFICULTY_KEY = "difficulty";
    const string LEVEL_KEY = "level_unlocked_";
    
	public static void SetMasterVolume(float volume) {
        if(volume >= 0f && volume <= 1f) {
            PlayerPrefs.SetFloat(MASTER_VOLUME_KEY, volume);
        } else {
            Debug.LogError("Volume was set outside our 0-1f range: " + volume);
        }

    }

    public static float GetMasterVolume() {
        return PlayerPrefs.GetFloat(MASTER_VOLUME_KEY);
    }

    public static void UnlockLevel(int level) {
        if(level <= SceneManager.sceneCountInBuildSettings - 1) {
            PlayerPrefs.SetInt(LEVEL_KEY + level.ToString(), 1);
        }
        else {
            Debug.LogError("Level " + level + ": Does not exist in our build settings");
        }
    }

    public static bool IsLevelUnlocked(int level) {

        if (level <= SceneManager.sceneCountInBuildSettings - 1) {
            int levelStatus = PlayerPrefs.GetInt(LEVEL_KEY + level.ToString());
            return (levelStatus == 1);
        }
        else {
            Debug.LogError("Level " + level + ": Does not exist in our build settings");
            return false;
        }
    }

    public static void SetDifficulty(int difficulty) {
        if(difficulty >= 0 && difficulty <= 3) {
            PlayerPrefs.SetInt(DIFFICULTY_KEY, difficulty);
        }
        else {
            Debug.LogError("difficulty = " + difficulty + ", is not a valid difficulty");
        }
    }

    public static int GetDifficulty() {
        return PlayerPrefs.GetInt(DIFFICULTY_KEY);
    }

    public static void ResetSettings() {
        PlayerPrefs.SetFloat(MASTER_VOLUME_KEY, 1.0f);
        PlayerPrefs.SetInt(DIFFICULTY_KEY, 1);
    }

    public static void LockAllLevels() {
        //For level= 1; level <= App.levelCount - 'number of non-level levels'; levels++
        //  PlayerPrefs.SetInt(KEY + level, 0);
    }
}
