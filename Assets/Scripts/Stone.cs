using UnityEngine;
using System.Collections;

public class Stone : MonoBehaviour {

    public bool beingAttacked = false;
    private Animator animator;

	// Use this for initialization
	void Start () {
        animator = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void Flinch() {
        animator.SetTrigger("tookDamage");
    }
}
