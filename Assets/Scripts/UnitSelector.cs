using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UnitSelector : MonoBehaviour {

    public Color deselectedColor;
    public Color insufficientFundsColor;

    private GameObject defender;
    private GameObject unitCard;

    private bool sufficientStars;
    private static GameObject selectedDefender;
    private UnitSelector[] unitSelectors;
    private Image spriteImage;

    void Start() {
        unitSelectors = FindObjectsOfType<UnitSelector>();
    }

    public void InitializeUnitCard(DefenderCard thisUnit, GameObject cardPrefab) {
        unitCard = Instantiate(cardPrefab) as GameObject;
        unitCard.transform.SetParent(transform, false);

        GameObject unitSprite = unitCard.transform.Find("UnitSprite").gameObject;
        if(unitSprite) {
            spriteImage = unitSprite.GetComponent<Image>();
            spriteImage.sprite = thisUnit.sprite;
            spriteImage.color = deselectedColor;
        }

        GameObject unitCost = unitCard.transform.Find("UnitCost").gameObject;
        if(unitCost) {
            unitCost.GetComponent<Text>().text = thisUnit.defender.starsCost.ToString();
        }

        defender = thisUnit.defender.gameObject;
        Debug.Log("Step1");
        sufficientStars = false;
    }

    void OnMouseDown() {
        foreach(UnitSelector selector in unitSelectors) {
            if(selector.isSufficientStars()) {
                selector.UpdateSpriteColor(deselectedColor);
            }
        }
        if(sufficientStars) {
            spriteImage.color = Color.white;
            selectedDefender = defender;
        }
        else {
            //Debug.Log("Insufficient Funds");
            selectedDefender = null;
        }
    }

    public bool isSufficientStars() {
        return sufficientStars;
    }

    public static GameObject GetSelectedDefender() {
        return selectedDefender;
    }

    public void UpdateAffordability(int availableFunds) {
        if(!defender) {
            //Debug.Log("No Defender on: " + name);
            return;
        }
        int unitCost = defender.GetComponent<Defender>().starsCost;
        if (availableFunds >= unitCost) {
            //Flag as buyable
            if (spriteImage.color != Color.white) {
                spriteImage.color = deselectedColor;
            }
            sufficientStars = true;
        }
        else {
            //Flag as Unbuyable
            if (spriteImage.color == Color.white) {
                selectedDefender = null;
            }
            spriteImage.color = insufficientFundsColor;
            sufficientStars = false;

            //Debug.Log(name);
        }
    }

    public void UpdateSpriteColor(Color newColor) {
        spriteImage.color = newColor;
    }
}
