using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DefenderSelector : MonoBehaviour {

    public GameObject unitSelectorPrefab;
    public DefenderCard[] usableDefenders;

    void Awake() {
        //Loop through our available UnitSelector slots in the GUI
        for (int i = 0; i < transform.childCount; i++) {
            if (i < usableDefenders.Length) {
                GameObject unitSlot = transform.GetChild(i).gameObject;

                UnitSelector thisUnitSelector = unitSlot.GetComponent<UnitSelector>();
                if (thisUnitSelector) {
                    unitSlot.GetComponent<UnitSelector>().InitializeUnitCard(usableDefenders[i], unitSelectorPrefab);
                }
                /*
                //Transform unitSlot = transform.GetChild(i);
                GameObject thisUnit = Instantiate(unitSelectorPrefab) as GameObject;
                thisUnit.transform.SetParent(unitSlot.transform, false);

                //Set the sprite to be displayed on the unit card.
                GameObject unitSprite = thisUnit.transform.Find("UnitSprite").gameObject;
                if(unitSprite) {
                    unitSprite.GetComponent<Image>().sprite = usableDefenders[i].sprite;
                }

                //Set the star cost of the defender to be displayed on the unit card.
                GameObject unitCost = thisUnit.transform.Find("UnitCost").gameObject;
                if(unitCost) {
                    unitCost.GetComponent<Text>().text = usableDefenders[i].defender.starsCost.ToString();
                }
                */
                //thisUnit.GetComponent<Image>().sprite = usableDefenders[i].sprite;
            }
        }
    }
}
