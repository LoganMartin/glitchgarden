using UnityEngine;
using System.Collections;

public class AttackerSpawn : MonoBehaviour {

    public GameObject[] possibleAttackers;

    private float totalSeconds = 0;
    void Start() {

    }

    void Update() {
        totalSeconds += Time.deltaTime;
        /*if(totalSeconds >= 30f) {
            Debug.Log("30 Seconds Hit");
        }*/
        for (int i = 0; i < possibleAttackers.Length; i++) {
            if (IsTimeToSpawn(possibleAttackers[i])) {
                SpawnAttacker(possibleAttackers[i]);
            }
        }

    }


    void OnDrawGizmos() {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, 0.5f);
    }

    void SpawnAttacker(GameObject attacker) {
        //Debug.Log(attacker.name + " Spawned");
        GameObject newAttacker = Instantiate(attacker, transform.position, Quaternion.identity) as GameObject;
        newAttacker.transform.parent = this.transform;
    }

    bool IsTimeToSpawn(GameObject attackerGameObject) {

        float attackerSpawnFreq = attackerGameObject.GetComponent<Attacker>().spawnDelay;
        float threshold = 1 / attackerSpawnFreq * Time.deltaTime / 5;

        /*SpawnThreshold spawnThresh = attackerGameObject.GetComponent<Attacker>().spawnThreshold;
        float spawnTime = Random.value * (spawnThresh.maxSpawnDelay - spawnThresh.minSpawnDelay) + spawnThresh.minSpawnDelay;
        spawnTime = 1 / spawnTime;
        float threshold = spawnTime * Time.deltaTime;*/

        //Debug.Log(attackerGameObject.name + "Spawn Roll: " + spawnTime);

        if (Random.value < threshold) {
            return true;
        }
        else {
            return false;
        }
    }
}
