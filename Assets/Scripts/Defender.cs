using UnityEngine;
using System.Collections;

public class Defender : MonoBehaviour {

    public enum DefenderState {
        idle,
        attacking
    }

    public DefenderState state;
    public int starsCost;

    private float timeSinceLastAttack;
    
	// Use this for initialization
	void Start () {
        state = DefenderState.idle;
	}

	void Update () {
	}

    void OnTriggerEnter2D(Collider2D collider) {

        Attacker attacker = collider.gameObject.GetComponent<Attacker>();
        if(attacker) {
            //Debug.Log("Defender Hit: " + this.name + " by " + collider.name);
        }
    }
}
