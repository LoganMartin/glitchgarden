using UnityEngine;
using System.Collections;

public class PlayerPrefManager_Test : MonoBehaviour {

	// Use this for initialization
	void Start () {
        print("Master Volume: " + PlayerPrefsManager.GetMasterVolume());
        PlayerPrefsManager.SetMasterVolume(0.3f);
        print("Volume Updated");
        print("Master Volume: " + PlayerPrefsManager.GetMasterVolume());

        print("Level 2 Unlocked: " + PlayerPrefsManager.IsLevelUnlocked(2));
        PlayerPrefsManager.UnlockLevel(2);
        print("Level 2 Updated");
        print("Level 2 Unlocked: " + PlayerPrefsManager.IsLevelUnlocked(2));

        print("Difficulty Setting: " + PlayerPrefsManager.GetDifficulty());
        PlayerPrefsManager.SetDifficulty(2);
        print("Difficulty Updated");
        print("Difficulty Setting: " + PlayerPrefsManager.GetDifficulty());

        PlayerPrefsManager.ResetSettings();
        print("Settings Reset");
        print("Master Volume: " + PlayerPrefsManager.GetMasterVolume());
        print("Difficulty Setting: " + PlayerPrefsManager.GetDifficulty());
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
