using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour {

    [Range(-1f, 10f)]
    public float velocity = 1f;

    public float damage = 10f;

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {
        //Vector3 newPosition = new Vector3(transform.position.x + velocity * Time.deltaTime, transform.position.y, transform.position.z);
        //transform.position = newPosition;
        transform.Translate(Vector3.right * velocity * Time.deltaTime);

        //If the projectile goes out of our playspace, it's destroyed.
        if(transform.position.x > 15) {
            Destroy(gameObject);
        }
    }

    void OnTriggerEnter2D(Collider2D collider) {

        Attacker attacker = collider.gameObject.GetComponent<Attacker>();
        if(attacker) {
            //Debug.Log("Attacker Collision: " + this.name + " by " + collider.name);
        }
    }

    public void fireProjectile() {

    }

    public void DestroyProjectile() {
        Destroy(gameObject);
    }
}
