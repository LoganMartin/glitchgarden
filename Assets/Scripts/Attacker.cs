using UnityEngine;
using System.Collections;


[System.Serializable]
public class SpawnThreshold {
    [Tooltip("Minimum time in seconds between spawns")]
    public float minSpawnDelay;
    [Tooltip("Maximum time in seconds between spawns")]
    public float maxSpawnDelay;
}

[RequireComponent (typeof (Rigidbody2D))]
[RequireComponent(typeof(Animator))]
public class Attacker : MonoBehaviour {

    public enum AttackerState {
        spawning,
        walking,
        attacking
    }

    public AttackerState state;
    [Range (-1f, 5f)]
    public float walkSpeed = 0.5f;

    public float attackDamage = 1f;
    public float spawnDelay = 2f;
    //public SpawnThreshold spawnThreshold;

    private GameObject currentTarget;
    private Animator anim;
    private NPCHealth myHealth;
	// Use this for initialization
	void Start () {
        anim = GetComponent<Animator>();
        myHealth = GetComponent<NPCHealth>();
	}
	
	// Update is called once per frame
	void Update () {
        if(state == AttackerState.walking) {
            transform.Translate(Vector3.left * walkSpeed * Time.deltaTime);
        }

        if(!currentTarget) {
            anim.SetBool("isAttacking", false);
        }
	}

    void OnTriggerEnter2D(Collider2D collider) {
        Projectile missile = collider.gameObject.GetComponent<Projectile>();
        if(missile) {
            //Take damage
            myHealth.ReduceCurrentHealth(missile.damage);
            missile.DestroyProjectile();
            //Debug.Log( this.name + " takes " + missile.damage + " damage from " + collider.name);
        }
    }

    public void SetState(AttackerState newState) {
        state = newState;
    }

    public void SetTarget(GameObject target) {
        currentTarget = target;
    }

    //Called from the animator at time of damaging strike/blow;
    public void AttackTarget() {
        if(currentTarget) {
            if (currentTarget.GetComponent<Stone>()) {
                currentTarget.GetComponent<Stone>().Flinch();
            }

            currentTarget.GetComponent<NPCHealth>().ReduceCurrentHealth(attackDamage);
            //Debug.Log(this.name + " dealt " + attackDamage + " damage to " + currentTarget.name);
        }
        else {
            anim.SetBool("isAttacking", false);
            //Debug.LogError("Called AttackingTarget() on :" + this.name + " which has no currentTarget");
        }
    }

    public void SetSpeed(float newSpeed) {
        walkSpeed = newSpeed;
    }

}
