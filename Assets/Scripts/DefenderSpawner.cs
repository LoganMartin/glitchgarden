using UnityEngine;
using System.Collections;

public class DefenderSpawner : MonoBehaviour {

    private Camera gameCamera;
    private GameObject spawnParent;
    private StarDisplay starGUI;
	// Use this for initialization
	void Start () {
        gameCamera = FindObjectOfType<Camera>();
        starGUI = FindObjectOfType<StarDisplay>();
        spawnParent = GameObject.Find("Defenders");

        if(!spawnParent) {
            spawnParent = new GameObject("Defenders");
        }

        if(!starGUI) {
            Debug.LogError("No 'StarDisplay' Object found in Scene by 'DefenderSpawner'");
        }

    }

    void OnMouseDown() {
        //Debug.Log(Input.mousePosition);
        Vector3 clickedNode = GetClickedNode(Input.mousePosition);

        if(UnitSelector.GetSelectedDefender()) {
            int unitCost = UnitSelector.GetSelectedDefender().GetComponent<Defender>().starsCost;
            //TODO: Determine if a defender is already at clicked location.
            //Possible Solutions: 
            //  increase collision box to fill whole node/square (Easier solution)
            //  Store list of currently spawned defenders, and reference that. (Better solution for animation reasons).

            if (starGUI.StarsAvailable(unitCost)) {
                SpawnDefenderAt(UnitSelector.GetSelectedDefender(), clickedNode);
                starGUI.UseStars(unitCost);
            }
            else {
                Debug.Log("InsufficientFunds!");
            }
        }
        else {
            Debug.Log("No Defender Selected!");
        }
        //Debug.Log(clickedNode);
        //Spawn defender at clickedNode
    }


    /// <summary>
    /// Takes a coordinate in pixels, and returns the correlating location in World Units
    /// rounded to the nearest grid point our game logic uses.
    /// </summary>
    /// <param name="clickedPx">Coordinate, in pixels, of the clicked location.</param>
    /// <returns></returns>
    Vector3 GetClickedNode(Vector3 clickedPx) {
        Vector3 clickedWorldUnit = gameCamera.ScreenToWorldPoint(clickedPx);

        //Compensates for mathf.round always going to the even number if 0.5
        if(clickedWorldUnit.x == 0.5f) {
            clickedWorldUnit.x += 0.05f;
        }
        if (clickedWorldUnit.y == 0.5f) {
            clickedWorldUnit.y += 0.05f;
        }


        float xPos = Mathf.Round(clickedWorldUnit.x);
        float yPos = Mathf.Round(clickedWorldUnit.y);

        return new Vector3(xPos, yPos, 0f);
    }

    /// <summary>
    /// Instantiates a Defender Prefab at given coordinates
    /// </summary>
    /// <param name="defender">Prefab of defender to be spawned</param>
    /// <param name="coords">Coordinates to Instantiate our Defender at</param>
    public void SpawnDefenderAt(GameObject defender, Vector3 coords) {
        GameObject newDefender = Instantiate(defender, coords, Quaternion.identity) as GameObject;
        newDefender.transform.parent = spawnParent.transform;
        LevelController.instance.UpdateStat(StatType.DefendersSpawned, 1);
    }
}
