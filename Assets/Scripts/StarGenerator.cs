using UnityEngine;
using System.Collections;

public class StarGenerator : MonoBehaviour {

    public int starValue = 10;

    private StarDisplay starGUI;
	// Use this for initialization
	void Start () {
        starGUI = FindObjectOfType<StarDisplay>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void CollectStars() {
        if(starGUI) {
            starGUI.AddStars(starValue);
        }
        else {
            Debug.LogError("No 'StarDisplay' Object found in Scene");
        }
    }
}
