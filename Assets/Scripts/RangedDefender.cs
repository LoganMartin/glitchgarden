using UnityEngine;
using System.Collections;

public class RangedDefender : MonoBehaviour {

    public GameObject projectileType;

    [Range(0f, 10f)]
    public float fireRate = 1f;
    public enum DefenderState {
        idle,
        attacking
    }
    public DefenderState state;
    public bool attacked = false;

    private Animator anim;
    private float timeSinceLastAttack;
    private GameObject projectileParent;
    private AttackerSpawn attackSpawner;

    // Use this for initialization
	void Start () {
        timeSinceLastAttack = 0;
        state = DefenderState.idle;

        anim = GetComponent<Animator>();
        SetMyLaneSpawner();

        projectileParent = GameObject.Find("Projectiles");

        if(!projectileParent) {
            projectileParent = new GameObject("Projectiles");
        }
	}
	
	// Update is called once per frame
	void Update () {
        timeSinceLastAttack += Time.deltaTime;

        if (AttackerAhead()) {
            anim.SetBool("isAttacking", true);
        }
        else {
            anim.SetBool("isAttacking", false);
        }

        if(state == DefenderState.attacking) {
            Attack();
        }
    }

    void SetMyLaneSpawner() {
        GameObject attackSpawners = GameObject.Find("Attacker Spawns");
        foreach(Transform spawner in attackSpawners.transform) {
            if(spawner.position.y == transform.position.y) {
                //Debug.Log("Found Spawner: " + transform.position.y);
                attackSpawner = spawner.gameObject.GetComponent<AttackerSpawn>();
            }
        }

        if(!attackSpawner) {
            Debug.LogError("No Spawner found in Lane: " + transform.position.y);
        }

    }

    void Attack() {
        if (timeSinceLastAttack >= 1 / fireRate) {
            FireProjectile();
        }
    }

    void FireProjectile() {
        Vector3 startPosition = transform.position + new Vector3(0.25f, 0, 0);
        GameObject firedProj = Instantiate(projectileType, startPosition, Quaternion.identity) as GameObject;
        firedProj.transform.parent = projectileParent.transform;
        timeSinceLastAttack = 0;
    }

    public void SetState(DefenderState newState) {
        state = newState;
    }

    /// <summary>
    /// Returns true if there is an attacker ahead of this defender in its lane, else false;
    /// </summary>
    /// <returns>Bool: Is an attacker ahead</returns>
    bool AttackerAhead() {

        //Scan this lane's attackerSpawn for any child gameobjects.
        //If one exists, and is ahead of our defender, return true;
        foreach(Transform attacker in attackSpawner.transform) {
            if(attacker.position.x > transform.position.x) {
                return true;
            }
        }
        return false;
    }
}
