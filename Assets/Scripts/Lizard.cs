using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Attacker))]
public class Lizard : MonoBehaviour {

    private Attacker attacker;
    private Animator anim;

    // Use this for initialization
    void Start () {
        anim = GetComponent<Animator>();
        attacker = GetComponent<Attacker>();
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter2D(Collider2D collider) {

        GameObject obj = collider.gameObject;

        if (!obj.GetComponent<Defender>()) {
            return;
        }

        //Set our target to the defender we collided with, and attack;
        attacker.SetTarget(obj);
        anim.SetBool("isAttacking", true);
    }
}
