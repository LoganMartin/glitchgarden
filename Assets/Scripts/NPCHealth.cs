using UnityEngine;
using System.Collections;

public class NPCHealth : MonoBehaviour {

    public float maxHealth = 100.0f;
    public float currentHealth;

    void Start() {
        currentHealth = maxHealth;
    }

    public void ReduceCurrentHealth(float damageTaken) {
        float newHealth = currentHealth - damageTaken;

        if(newHealth <= 0) {
            DestroyNPC();
        }
        else {
            currentHealth = newHealth;
        }
        
    }

    public void IncreaseCurrentHealth(float healing) {
        currentHealth += healing;
    }

    public float GetCurrentHealth() {
        return currentHealth;
    }

    public void DestroyNPC() {
        //Do something indicating death of character, animation/sfx/etc.

        if(GetComponent<Attacker>()) {
            //Increment attackers defeated stat.
            LevelController.instance.UpdateStat(StatType.AttackersDefeated, 1);
        }

        Destroy(gameObject);
    }
}
