using UnityEngine;
using System.Collections;

[System.Serializable]
public class LevelParameters {

    public int attackersBeforeLose = 5;
    public int startingStars = 20;
    public float timeToWin = 300;
}
