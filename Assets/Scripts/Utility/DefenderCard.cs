﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class DefenderCard {
    public Defender defender;
    public Sprite sprite;
}
