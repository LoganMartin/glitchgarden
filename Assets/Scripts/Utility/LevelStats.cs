using UnityEngine;
using System.Collections;

[System.Serializable]

public enum StatType {
    AttackersDefeated,
    DefendersSpawned,
    StarsCollected,
    StarsSpent,
    AttackersMissed
}
public class LevelStats {

    public int attackersDefeated = 0;
    public int defendersSpawned = 0;
    public int starsCollected = 0;
    public int starsSpent = 0;
    public int attackersMissed = 0;
}
