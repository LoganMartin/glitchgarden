using UnityEngine;
using System.Collections;

public class DisplayCameraFrame : MonoBehaviour {

    public bool display = true;
	void OnDrawGizmos() {
        float verticalHeightSeen = Camera.main.orthographicSize * 2.0f;

        if(display) {
            Gizmos.color = Color.magenta;
            Gizmos.DrawWireCube(transform.position, new Vector3((verticalHeightSeen * Camera.main.aspect), verticalHeightSeen, 0));
        }
        
    }
}
