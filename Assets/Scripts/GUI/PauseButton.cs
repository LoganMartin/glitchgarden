using UnityEngine;
using System.Collections;

public class PauseButton : MonoBehaviour {

	public void ToggleGamePause() {
        LevelController.instance.TogglePause();
    }
}
