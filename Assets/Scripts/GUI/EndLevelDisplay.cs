using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class EndLevelDisplay : MonoBehaviour {

    public enum EndDisplayState {
        Continue,
        Win,
        Lose
    }

    public string loseHeader = "You've been overrun!";
    public string winHeader = "Level Survived!";

    public Text headerText;

    public Text attackersDefeatedCount;
    public Text defendersSpawnedCount;
    public Text starsCollectedCount;
    public Text attackersMissedCount;

    private LevelStats levelStats;

    private EndDisplayState currentState;

	// Use this for initialization
	void Start () {
        currentState = EndDisplayState.Continue;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void SetToLoseDisplay() {
        currentState = EndDisplayState.Lose;
        headerText.text = loseHeader;
    }

    public void SetToWinDisplay() {
        currentState = EndDisplayState.Win;
        headerText.text = winHeader;
    }

    public void SetToContinueDisplay() {
        currentState = EndDisplayState.Continue;
        headerText.text = winHeader;
    }

   public void UpdateStats(LevelStats stats) {
        levelStats = stats;
        UpdateStatsDisplay();
    }

    void UpdateStatsDisplay() {
        attackersDefeatedCount.text = levelStats.attackersDefeated.ToString();
        defendersSpawnedCount.text = levelStats.defendersSpawned.ToString();
        starsCollectedCount.text = levelStats.starsCollected.ToString();
        attackersMissedCount.text = levelStats.attackersMissed.ToString();
    }

    public void ContinueClicked() {
        switch(currentState) {
            case EndDisplayState.Win:
                LevelManager.LoadLevel("Win");
                break;
            case EndDisplayState.Continue:
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
                break;
            case EndDisplayState.Lose:
                LevelManager.LoadLevel("Lose");
                break;
            default:
                break;
        }
    }

    public void ShowDisplay() {
        gameObject.SetActive(true);
    }

    public void HideDisplay() {
        gameObject.SetActive(false);
    }
}
