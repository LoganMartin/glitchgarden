using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

[RequireComponent(typeof(Button))]
public class ButtonHelper : MonoBehaviour, IPointerDownHandler, IPointerUpHandler {

    public Vector3 buttonPressedOffset;

    private GameObject internalElement;
    private Button thisButton;
    private Vector3 unpressedPosition;

	// Use this for initialization
	void Start () {
        thisButton = GetComponent<Button>();
        internalElement = transform.GetChild(0).gameObject;

        if(internalElement) {
            unpressedPosition = internalElement.transform.localPosition;
            return;
        }
    }

    public void OnPointerDown(PointerEventData eventData) {
        if(thisButton.interactable) {
            internalElement.transform.localPosition = buttonPressedOffset;
        }

    }

    public void OnPointerUp(PointerEventData eventData) {
        if(thisButton.interactable) {
            internalElement.transform.localPosition = unpressedPosition;
        }
    }
	
}
