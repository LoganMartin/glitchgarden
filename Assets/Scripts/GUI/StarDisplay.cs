using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[RequireComponent(typeof(Text))]
public class StarDisplay : MonoBehaviour {

    // Use this for initialization
    private Text starsText;
    private int totalStars = 0;
    private UnitSelector[] unitSelectors;

    /// <summary>
    /// Initializes our star display to start the level with x stars, and updates the
    /// unit affordabilities based on our starting value.
    /// </summary>
    /// <param name="startingStars">Number of stars to start the level with</param>
    public void InitializeDisplay(int startingStars) {
        starsText = GetComponent<Text>();
        totalStars = startingStars;
        UpdateStarsDisplay();
        unitSelectors = FindObjectsOfType<UnitSelector>();
        UpdateUnitAffordability();
    }

    /// <summary>
    /// Increases totalStars property by the 'amount' parameter.
    /// Clamped between 0 and 9999. Refreshes UI Display.
    /// </summary>
    /// <param name="amount">Number to increase totalStars property</param>
    public void AddStars(int amount) {
        if (totalStars < 9999) {
            totalStars += amount;
            if(totalStars > 9999) {
                totalStars = 9999;
            }
            UpdateStarsDisplay();
            UpdateUnitAffordability();
            LevelController.instance.UpdateStat(StatType.StarsCollected, amount);
        }
    }

    /// <summary>
    /// Decreases totalStars property by the 'amount' parameter
    /// Clamped between 0 and 9999. Refreshes UI Display;
    /// </summary>
    /// <param name="amount">Number to decrease totalStars property</param>
    public void UseStars(int amount) {
        if(StarsAvailable(amount)) {
            totalStars -= amount;
        }
        UpdateStarsDisplay();
        UpdateUnitAffordability();
        LevelController.instance.UpdateStat(StatType.StarsSpent, amount);
    }

    /// <summary>
    /// Updates/Refreshes starsText.text value to display current totalStars value.
    /// </summary>
    void UpdateStarsDisplay() {
        if(starsText) {

            starsText.text = totalStars.ToString();
        }
    }

    /// <summary>
    /// Calls UnitSelector.UpdateAffordability() on every available UnitSelector in the scene.
    /// </summary>
    void UpdateUnitAffordability() {
        Debug.Log("Updating Affordability: " + unitSelectors.Length);
        for (int i=0; i<unitSelectors.Length; i++) {
            unitSelectors[i].UpdateAffordability(totalStars);
        }
    }

    /// <summary>
    /// Checks if the totalStars is >= the 'stars' parameter;
    /// Returns true or false based on results;
    /// </summary>
    /// <param name="stars">Number of stars to check if available.</param>
    /// <returns></returns>
    public bool StarsAvailable(int stars) {
        return (totalStars >= stars);
    }
}
