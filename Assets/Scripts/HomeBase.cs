using UnityEngine;
using System.Collections;

public class HomeBase : MonoBehaviour {

    void OnTriggerEnter2D(Collider2D collider) {
        Attacker attacker = collider.gameObject.GetComponent<Attacker>();

        if(attacker) {
            Destroy(attacker.gameObject);
            LevelController.instance.HomeBaseReached();
        }
    }
}
